import sys


def get_number_from_file(filename):
    try:
        fp = open(filename,'r')
    except:
        return 0
    lines = fp.readlines()
    fp.close()

    n = 0

    for line in lines:
        if line.startswith('abducible ') or line.startswith('reducible ') or line.startswith('optimizable ') or line.startswith('edge'):
            n = n + 1

    return n

if __name__ == '__main__':
    # basedir = "abductive"
    # basedir = "reducible"
    # basedir = "optimizable"
    basedir = "standard"
    prefix_name = "prob_path"
    init_size = 10
    end_size = 100
    n_intances = 50
    step_size = 5

    for size in range(init_size,end_size,step_size):
        c = 0
        for instance in range(n_intances):
            c = c + get_number_from_file(f"{basedir}/{size}/{prefix_name}_{size}_{instance}.lp")
            c = c + get_number_from_file(f"{basedir}/{size}/{prefix_name}_active_{size}_{instance}.lp")

        print(f"{size} & {c / n_intances} \\\ ")
