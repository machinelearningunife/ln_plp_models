# Requisites
- Install SWI Prolog: https://www.swi-prolog.org/download/stable

- Install cplint: https://github.com/friguzzi/cplint

- Install the tools for probabilistic optimizable logic programs: https://bitbucket.org/machinelearningunife/polp_experiments/src/master/

- Install the tools for probabilistic reducible logic programs: https://bitbucket.org/machinelearningunife/prlp_experiments/src/master/

# Models
- PEP (standard)
- Abductive
- Reducible
- Optimizable